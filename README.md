# Halloween - Jäger Bomber

A ticking (blinking) bomb imitation for halloween. Pressing a button will light up the whole light strip.

This requires a programmable LED strip. The connection is:

- Strip `+5V`: ESP8266 `VU`
- Strip `GND`: ESP8266 `G`
- Strip `Din`: ESP8266 `D4`

