from machine import Pin
from neopixel import NeoPixel
from time import sleep
from urandom import *


####################################################################################################
####################################################################################################
####################################################################################################
def randrange(start, stop=None):
    if stop is None:
        stop = start
        start = 0
    upper = stop - start
    bits = 0
    pwr2 = 1
    while upper > pwr2:
        pwr2 <<= 1
        bits += 1
    while True:
        r = getrandbits(bits)
        if r < upper:
            break
    return r + start


def randint(start, stop):
    return randrange(start, stop + 1)


####################################################################################################
####################################################################################################
####################################################################################################


NUM_LEDS = 15
pin = Pin(2, Pin.OUT)
np = NeoPixel(pin, NUM_LEDS)

flash_pin = Pin(0, Pin.IN)

default_color = (100, 0, 0)


def on(led_id=None, color=default_color):
    if led_id == None:
        for i in range(0, NUM_LEDS):
            np[i] = color
    else:
        np[led_id] = color

    np.write()


def off(led_id=None):
    if led_id == None:
        for i in range(0, NUM_LEDS):
            np[i] = (0, 0, 0)
    else:
        np[led_id] = (0, 0, 0)
    np.write()


def flash(led_id=None, color=default_color):
    on(led_id, color)
    sleep(0.15)
    off(led_id)


def explode():
    """Flash all leds rapidly"""
    for i in range(0, 10):
        on(None, (200, 0, 0))
        sleep(0.1)
        off(None)
        sleep(0.1)


def explode2():
    off()
    for i in range(0, randint(2, 4)):
        for i in range(0, NUM_LEDS):
            on(i, (200, 0, 0))
            sleep(0.01)
            off(i)

        for i in range(NUM_LEDS - 2, -1, -1):
            on(i, (200, 0, 0))
            sleep(0.01)
            off(i)


def check_explosion():
    if flash_pin.value() == 0:
        explosion_style = randint(0, 5)
        if explosion_style in range(0, 4):
            explode()
        elif explosion_style in range(4, 6):
            explode2()
        else:
            return False

        return True

    return False


def sleep_with_interrupt(seconds_to_sleep):
    """
    Returns True if there was an interrupt and explosion. False if no interrupt.
    :param seconds_to_sleep:
    :return:
    """
    slept_seconds = 0
    while slept_seconds < seconds_to_sleep:
        if check_explosion():
            return True

        sleep(0.01)
        slept_seconds += 0.01
    return False


def do_tick_flash():
    led_id = randint(0, NUM_LEDS - 1)
    flash_count_rnd = randint(0, 15)

    flashes = {
        (0, 1, 2, 3, 4, 5, 6, 7,): 1,
        (8, 9, 10, 11, 12): 2,
        (13, 14): 3,
        (15,): 4
    }

    flash_count = 1
    for rng, count in flashes.items():
        if flash_count_rnd in rng:
            flash_count = count
            break

    for i in range(0, flash_count):
        flash(led_id)
        if sleep_with_interrupt(0.15):
            return


while True:
    random_sleep = randint(500, 3000) / 1000
    was_interrupted = sleep_with_interrupt(random_sleep)

    if not was_interrupted:
        do_tick_flash()
